<?php

session_start();
$page = $_GET['page'];

if(!empty($_SESSION['logged'])) {
  switch ($_SESSION['role']) {
    case "profesor":
      include "content/profesor.php";
      break;
    case "parinte":
      include "content/parinte.php";
      break;
    case "elev":
      include "content/elev.php";
      break;
    case "admin":
      include "content/admin.php";
  }
}
else {
  if($page != "homepage") {
    header("Location: /catalog/homepage");
    exit;    
  }
  
  include "content/login.template.html";
}

//print_r($page);

switch($page) {
  case "a":
    include "a.php";
    break;
  case "b":
    include "b.php";
    break;
  case "c":
    include "c.php";
    break;
  case "homepage":
    include "homepage.php";
    break;
  default:
    include "not_found.php";
}